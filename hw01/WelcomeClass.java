//WelcomeClass Homework #1
public class WelcomeClass {
  
  public static void main(String args[]){
    //prints welcome message using Lehigh ID and email
    System.out.println("   -----------\n   | WELCOME |\n   -----------\n  ^  ^  ^  ^  ^  ^\n / \\/ \\/ \\/ \\/ \\/ \\ \n<-A--J--C--4--1--8->\n \\ /\\ /\\ /\\ /\\ /\\ /\n  v  v  v  v  v  v");
  }
}