/* Andrew Conboy - CSE002 Lab04 - Card Generator
This program generates a random card from a standard deck of 52 cards and 
returns the number and suit of that card.*/
public class CardGenerator {
  public static void main(String args[]){
    
    //generates a random number between 1-52, inclusive. 
      int randomNumber = (int) (Math.random() * 52 +1);
    
    //this prints the random number (1-52), and its assigned suit and card number.
  System.out.println("Your random number was "+randomNumber+". "+"You picked the "+assignNumber(randomNumber)+" of "+assignSuit(randomNumber));
  }
  
  /*this method takes the random number assigned in the main method and assigns
  a suit based on where the number falls.*/
  public static String assignSuit (int card) {
    String suit;
    if (card <=13){
      suit = "diamonds";
    }
    else if (card <=26 && card>=14){
      suit = "clubs";
    }
    else if (card <=39 && card>=27){
      suit = "hearts";
    }
    else {
      suit = "spades";
    }
    return suit;
  }
  
  /*this method takes the random number, divides it by 14, and takes the remainder
  to assign a card number. Since 13 cards are in each suit, %14 was used. */
  public static String assignNumber (int num){
    String cardNumber="null";
    int adjustedNum = num%13;
    switch(adjustedNum){
      case 0: cardNumber="King";
        break;
      case 1: cardNumber= "Ace";
        break;
      case 2: cardNumber= "2";
        break;
      case 3: cardNumber= "3";
        break;
      case 4: cardNumber= "4";
        break;
      case 5: cardNumber= "5";
        break;
      case 6: cardNumber= "6";
        break;
      case 7: cardNumber= "7";
        break;
      case 8: cardNumber= "8";
        break;
      case 9: cardNumber= "9";
        break;
      case 10: cardNumber= "10";
        break;
      case 11: cardNumber= "Jack";
        break;
      case 12: cardNumber= "Queen";
        break;
    }
    return cardNumber;
  }
}