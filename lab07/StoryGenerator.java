import java.util.Random;
import java.util.Scanner;
public class StoryGenerator{
  public static void main (String [] args){
    Scanner sc = new Scanner(System.in);
      
    boolean check = false;
    while (check == false){
      System.out.println("The "+adjective() +" "+ subject()+" " +verb()+" the "+object());
      System.out.println("Do you want to print out a random sentence? Type 'Yes' or 'No'");
        String another = sc.nextLine();
      if (another.equals("yes")){
        check = false;
      }
      else if (another.equals("no")){
        System.out.println(createStory(subject()));
        check=true;
      }
      else {
        System.out.print("Please type either yes or no. ");
        check= false;
      }
      }
    
  }
    public static String adjective (){
      Random randomNum = new Random();
      int rNum = randomNum.nextInt(10);
      String adj = "";
      switch (rNum){
        case 0: adj="big";
          break;
        case 1: adj="small";
          break;
        case 2: adj="fast";
          break;
        case 3: adj="slow";
          break;
        case 4: adj="fat";
          break;
        case 5: adj="skinny";
          break;
        case 6: adj="tall";
          break;
        case 7: adj="short";
          break;
        case 8: adj="funny";
          break;
        case 9: adj="boring";
          break;
        default: adj="smelly";
      }
      return adj;
    }
    public static String subject (){
      Random randomNum = new Random();
      int rNum = randomNum.nextInt(10);
      String sub = "";
      switch (rNum){
        case 0: sub="bunny";
          break;
        case 1: sub="elephant";
          break;
        case 2: sub="ocean";
          break;
        case 3: sub="fish";
          break;
        case 4: sub="plastic water bottle";
          break;
        case 5: sub="cookie";
          break;
        case 6: sub="burrito";
          break;
        case 7: sub="corn stalk";
          break;
        case 8: sub="eggplant";
          break;
        case 9: sub="pumpkin";
          break;
        default: sub="sunflower";
      }
      return sub;
    }
    public static String verb (){
      Random randomNum = new Random();
      int rNum = randomNum.nextInt(10);
      String ver = "";
      switch (rNum){
        case 0: ver="ate";
          break;
        case 1: ver="stalked";
          break;
        case 2: ver="jumped";
          break;
        case 3: ver="played";
          break;
        case 4: ver="observed";
          break;
        case 5: ver="studied";
          break;
        case 6: ver="looked";
          break;
        case 7: ver="laughed";
          break;
        case 8: ver="punted";
          break;
        case 9: ver="fell";
          break;
        default: ver="skipped";
      }
      return ver;
    }
    public static String object (){
      Random randomNum = new Random();
      int rNum = randomNum.nextInt(10);
      String obj = "";
      switch (rNum){
        case 0: obj="cat";
          break;
        case 1: obj="baby";
          break;
        case 2: obj="book";
          break;
        case 3: obj="snow";
          break;
        case 4: obj="water";
          break;
        case 5: obj="sock";
          break;
        case 6: obj="movie";
          break;
        case 7: obj="train";
          break;
        case 8: obj="mouse";
          break;
        case 9: obj="house";
          break;
        default: obj="school";
      }
      return obj;
    }
  
    public static String sentenceTwo(String subj){
      return("This "+ subj+" was "+adjective()+" compared to "+object()+" that "+verb());
    }
    public static String sentenceThree(String subj2){
      return("That "+ subj2+" definitely "+verb()+" its "+ object());
    }
    public static String createStory(String subje){
      return(sentenceTwo(subje)+". "+sentenceThree(subje)+".");
    }
}