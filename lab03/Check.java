// Andrew Conboy - Lab #3 - Check.java - September 13th, 2018.
// This program takes the total cost of a dinner bill, calculates the desired amount of tip, and splits
// the check among a desired amount of people. 
import java.util.Scanner;
public class Check {
  public static void main(String args[]){
    Scanner sc = new Scanner(System.in);
    //user enters total bill they will pay
    System.out.println("What is your total bill? Enter it in the form xx.xx : ");
    double totalBill = sc.nextDouble();
    
    //user enters the percent tip that they desire to give
    System.out.println("What percent tip do you desire to give? Enter in the form xx : ");
    double tip = sc.nextDouble();
    
    //user enters the number of people in their party that will be splitting the check
    System.out.println("How many people are you going to split the check with? ");
    int people = sc.nextInt();
    
    //converts tip entered into a decimal form
    double actualTip=tip/100;
    
    //calculates and rounds tip amount based on bill total and desired tip.
    double tipAmount=Math.round(totalBill*actualTip*100.0)/100.0;
    //System.out.println("The amount of tip is: "+tipAmount);
    
    //adds the tip to the total bill
    double tipAndBill=tipAmount+totalBill;
    
    //rounds and prints the total bill to the nearest hundredth. 
    double roundedBill = Math.round(tipAndBill*100.0)/100.0;
    System.out.println("Your total bill with "+tip+"% tip is: "+roundedBill+".");
    
    //splits the bill among the desired amount of people, and rounds and prints their total.
    double splitBill = Math.round(roundedBill/people*100.0)/100.0;
    System.out.println("Each person in your group will have to pay $"+ splitBill);
    }
}