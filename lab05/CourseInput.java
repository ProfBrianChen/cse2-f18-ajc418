import java.util.Scanner;
public class CourseInput {
  public static void main (String [] args){
    Scanner sc = new Scanner(System.in);
    int number = 0, meetings = 0, students = 0;
    String course = "", instructor = "", department = "", time = "";
    
    boolean courseNumber = false;
    while (courseNumber==false){
      System.out.print("Enter the course number: ");
      courseNumber = sc.hasNextInt();
    if (courseNumber){
      number=sc.nextInt();
    }else{
      sc.next();
    }
 }
    
    boolean numMeetings = false;
    while (numMeetings==false){
      System.out.print("Enter the number of times you meet each week: ");
      numMeetings = sc.hasNextInt();
    if (numMeetings){
      meetings=sc.nextInt();
    }else{
      sc.next();
    }
 }
    
    boolean numStudents = false;
    while (numStudents==false){
      System.out.print("Enter the number of students : ");
      numStudents = sc.hasNextInt();
    if (numStudents){
      students=sc.nextInt();
    }else{
      sc.next();
    }
 }
    
    boolean courseTime = false;
    while (courseTime==false){
      System.out.print("Enter the time of the course: ");
      courseTime = sc.hasNext();
    if (courseTime){
      time=sc.next();
    }else{
      sc.next();
    }
 }
    
    boolean courseName = false;
    while (courseName==false){
      System.out.print("Enter the name of the course: ");
      courseName = sc.hasNext();
    if (courseName){
      course=sc.next();
    }else{
      sc.next();
    }
 }
    
    boolean courseInst = false;
   while (courseInst==false){
      System.out.print("Enter the name of your instructor for this course: ");
      courseInst = sc.hasNext();
    if (courseInst){
      instructor=sc.next();
    }else{
      sc.next();
    }
 }
    
    boolean courseDepartment = false;
    while (courseDepartment==false){
      System.out.print("Enter the department that the course is in: ");
      courseDepartment = sc.hasNext();
    if (courseDepartment){
      department=sc.next();
    }else{
      sc.next();
    }
 }
    
    System.out.println("Your class is "+course+" "+number+", taught by "+instructor+" from the "+department+" department. It meets "+meetings+ "times a week at "+time+" and has "+students+" students.");
    }
  }