//Andrew Conboy - HW07 - Word Tools
import java.util.Scanner;
public class HW07 {
  public static void main (String [] args){
    String input = sampleText();
    //System.out.println(input);
    printMenu(input);
  }
  
  public static String sampleText(){
    //create new scanner object, sc
    Scanner sc=new Scanner(System.in);
    
    //asks user to input a string and stores it in the String variable called input.
    System.out.println("Enter a string of your liking: ");
    String input = sc.nextLine();
    
    //prints the user's input string.
    System.out.println("You entered: "+input);
    
    return(input);
  }
  //prints out the working menu for the user to select.
  public static String printMenu(String in2){
    System.out.println("Select an item from the MENU \n c - Number of non-whitespace characters \n w - Number of words \n f - Find text \n r - Replace all !'s \n s - Shorten spaces \n q - Quit");
    Scanner sc2=new Scanner(System.in);
    String input3a="";
    char choice = sc2.next().charAt(0);
    if(choice=='c'){
      getNumOfNonWSCharacters(in2);
    }
    else if (choice=='w'){
      getNumOfWords(in2);
    }
    else if (choice=='r'){
      System.out.println(replaceExclamation(in2));
    }
    else if (choice=='s'){
      System.out.println(shortenSpace(in2));
    }
    else if (choice=='f'){
      findText(in2);
    }
    else {
      return "";
    }
    return "";
  }
  
  public static void getNumOfNonWSCharacters(String input){
    int nonWhiteSpace=0;
    /* These two lines of codes print the entire length of the user input, include white spaces.
    int number = input.length();
    System.out.println(number);*/
      for (int x=0; x<input.length(); x++){
        if(input.charAt(x)==' '){
          //System.out.print(input.charAt(x));
          nonWhiteSpace=nonWhiteSpace;
        }
        else {
          //System.out.print(input.charAt(x));
          nonWhiteSpace++;
        }
      }
    System.out.println("The number of non-white spaces in your inputted string is: "+ nonWhiteSpace);
  }
  public static void getNumOfWords(String input2){
    //this method calculates the number of words in the user inputted string.
    int words=1;
      for(int x=0; x<input2.length(); x++){
        if(input2.charAt(x)==' '){
          words++;
        }
        else {
          words=words;
        }
      }
    System.out.println("The number of words in your inputted string is: "+words);
  }
  public static void findText(String input3){
    //this method allows the user to input a string to search within their previously inputted string. It reutrns the number of occasions their searched word was found.
    int count = 0;
    Scanner sc3 = new Scanner(System.in);
    System.out.println("Enter a phrase or word you would like to search for in your string: ");
        String input3a = sc3.nextLine();
    
    int first = input3.indexOf(input3a);
    int last = input3.lastIndexOf(input3a);
    //System.out.println(first);
    if (first == input3.lastIndexOf(input3a)){
      System.out.println("Your word appeared once.");
    }
    else {
      while(last!=first){
      count++;
        if (first > 0){
	        count++;
        }
        else {
          System.out.println("Your word, "+input3a+", did not appear in the your string: "+input3);
        }
          first=input3.indexOf(input3a, first+1);
    break;
      }
      System.out.println("Your word showed up "+count+" times.");
  }
  public static String replaceExclamation(String input4){
    //this method replaces all exclamation points with periods.
    String replaced = input4.replaceAll("!",".");
    return replaced;
  }
  public static String shortenSpace(String input5){
    //this method replaces all multiple space occurances with a single space.
      input5=input5.replaceAll("  "," ");
      input5=input5.replaceAll("  "," ");
      input5=input5.replaceAll("  "," ");
      input5=input5.replaceAll("   "," ");
      input5=input5.replaceAll("    "," ");
      input5=input5.replaceAll("     "," ");
      input5=input5.replaceAll("      "," ");
      return input5;
  }
  
}