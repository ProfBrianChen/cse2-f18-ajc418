// Andrew Conboy HW06
// This program takes a user input number between 1 and 100, creates a square of Xs with that number, and creates a "space X" in the square. 
// On the homework assignment, the example states that its output is a 10x10 square. However, each row is actually 11 characters. This program will create rows of characters that match the user's input exactly.

import java.util.Scanner;
public class EncryptedX {
  public static void main (String [] args){
    Scanner sc=new Scanner(System.in);
    int squareSize=0;
    
    //checks if user enters int between 1-100
    boolean check=false;
    while (check==false){
      System.out.print("How big do you want your square to be?: ");
      check=sc.hasNextInt();
      if(check){
        squareSize=sc.nextInt();
        if (squareSize<=100&&squareSize>=1){
          break;
        }
        else{
          check=false;
        }
      }
      else {
        sc.next();
      }
    }

//space variable will help in creating a space in the output of Xs under certain conditions
int space=0;
    
    //creates user's number of rows
    for (int row=0; row<squareSize; row++){
      
      //creates user's number of columns
      for (int col=0; col<squareSize; col++){
        
        //checks if space variable equals column number. If it does, a space will be printed instead of an x. This will create a "slash" from top left to bottom right.
        if (space==col){
          System.out.print(" ");    
        }
        
        //checks if space variable equals user's input minus the column number minus 1. If it does, a space will be printed instead of an x. This will create a "slash" from top right to bottom left.
        else if (space==(squareSize-col-1)){
          System.out.print(" ");
        }
        else {
          System.out.print("X");
        }
      }
        System.out.println("");
      
      //increment space variable
      space+=1;
    }
  }
}