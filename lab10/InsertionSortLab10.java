import java.util.Arrays;
public class InsertionSortLab10 {
  public static void main(String[] args){
    int[] myArrayBest={1,2,3,4,5,6,7,8,9};
    int[] myArrayWorst={9,8,7,6,5,4,3,2,1};
    int iterBest = insertionSort(myArrayBest);
    int iterWorst = insertionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the sorted array: "+ iterBest);
    System.out.println("The total number of operations performed on the reverse sorted array: "+iterWorst);
  }
 
  /**The method for sorting the numbers*/
  public static int insertionSort(int[] list){
    //prints inital array
    System.out.println(Arrays.toString(list));
    //initialize counter for iterations
    int iterations =0;
    
    for (int i=1; i<list.length; i++){
      //update iteration counter
      iterations++;
      
      //Step one: find minimum in the list [i..list.length-1]
      for(int j=i; j>0; j--){
        if (list[j]<list[j-1]){
          int temp=list[j];
          list[j]=list[j-1];
          list[j-1]=temp;
          //iterations++;
        }
        else{
          break;
        }
        iterations++;
      }
      System.out.println(Arrays.toString(list));
    }
    return iterations;
  }
}