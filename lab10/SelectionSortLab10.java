import java.util.Arrays;
public class SelectionSortLab10 {
  public static void main(String[] args){
    int[] myArrayBest={1,2,3,4,5,6,7,8,9};
    int[] myArrayWorst={9,8,7,6,5,4,3,2,1};
    int iterBest = selectionSort(myArrayBest);
    System.out.println("The total number of operations performed on the sorted array: "+ iterBest);
    int iterWorst = selectionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the reverse sorted array: "+iterWorst);
  }
 
  /**The method for sorting the numbers*/
  public static int selectionSort(int[] list){
    //prints inital array
    System.out.println(Arrays.toString(list));
    //initialize counter for iterations
    int iterations =0;
    
    for (int i=0; i<list.length-1; i++){
      //update iteration counter
      iterations++;
      
      //Step one: find minimum in the list [i..list.length-1]
      int currentMin=list[i];
      int currentMinIndex=i;
      for(int j=i+1; j<list.length; j++){
        if (currentMin>list[j]){
          currentMin=list[j];
          currentMinIndex=j;
        }
        iterations++;
      }
      
      //Step two: Swap list[i] with the minimum you found above
      if(currentMinIndex!=i){
        int temp=list[i];
        list[i]=list[currentMinIndex];
        list[currentMinIndex]=temp;
        System.out.println(Arrays.toString(list));
      }
    }
    return iterations;
  }
}