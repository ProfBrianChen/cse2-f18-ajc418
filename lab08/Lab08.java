public class Lab08{
  public static void main (String []args){
    int[]array1=new int[100];
    int[]array2=new int[100];
    
    //this for loop assigns a random number between 0-99 to array1
    for (int x=0; x<array1.length; x++){
      array1[x]=((int)(Math.random()*100));
      //System.out.println(array1[x]);
    }
    //this array counts the number of occurances of a number in array 1, and stores that value in array2. 
    for (int y=0; y<array1.length; y++){
      array2[array1[y]]+=1;
    System.out.println(array1[y]+" occurs "+array2[y]+" times.");
    }
  }
}