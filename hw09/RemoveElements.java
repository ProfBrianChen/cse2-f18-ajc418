import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
	String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);

      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);

        System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);

      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
    }
 
  public static String listArray(int num[]){
    //prints array in an organized way.
      String out="{";
        for(int j=0;j<num.length;j++){
          if(j>0){
            out+=", ";
          }
          out+=num[j];
        }
        out+="} ";
        return out;
        }
  public static int [] randomInput(){
    //randomly fills an array with 10 elements.
    int [] randomArray=new int[10];
    for (int x=0; x<randomArray.length; x++){
      randomArray[x]=((int)((Math.random()*10)));
    }
    return randomArray;
  }
  public static int [] delete(int [] list, int pos){
    //removes an element from the array given a position provided by the user.
    Scanner scan2=new Scanner(System.in);
    boolean check2=false;
    if (pos <= list.length&& pos>=0){
          check2=true;
      }
    while(check2==false){
      System.out.println("Please try again with an integer in the bounds of the array: ");
        pos=scan2.nextInt();
      if (pos <= list.length&& pos>=0){
          check2=true;
      }
      else {
        scan2.next();
      }
    }
    int [] newArray=new int[list.length-1];
    for(int y=0; y<newArray.length; y++){
      if(y<pos){
        newArray[y]=list[y];
      }
      else {
        newArray[y]=list[y+1];
      }
    }
    return newArray;
  }
  public static int [] remove(int [] list, int target){
    //removes elements from an array if they match the value provided by the user.
    int [] shrunkArray=list;
    for (int z=0; z<shrunkArray.length; z++){
      if (shrunkArray[z]==target){
        shrunkArray=delete(shrunkArray, z);
      }
    }
    return shrunkArray;
  }
}
