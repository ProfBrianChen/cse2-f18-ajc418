// Andrew Conboy - HW09 - This program allows a user to input an array of grades, and then implements a binary search to find grades.
// It then shuffles the grades and implements linear search to find them. 
import java.util.Scanner;
import java.util.Random;
public class CSE2Linear {
  public static void main (String [] args){
    Scanner sc = new Scanner(System.in);
    
    int [] grades = new int[16];
    boolean check = false;
    boolean firstCheck= false;
    
    //Enter grades, one by one, and checks each to see if they are an int between 0-100 and if they are greater than the previous entry.
    System.out.println("Enter the first grade. Between 0-100, full number: ");
    while (firstCheck==false){
      if (sc.hasNextInt()==true){
        int temp=sc.nextInt();
          if(temp>=0&&temp<=100){
            grades[0]=temp;
            firstCheck=true;
            }
          else {
                System.out.print("Please try again with a number between 0 and 100, in ascending order. ");
          }
      }
      else {
            System.out.print("Please try again with an int. ");
            sc.next();
      }
    }
    for (int x=1; x<15; x++){
      check=false;
      System.out.println("Input the rest of the grades in ascending order - Grade "+(x+1)+"/15: ");
        
        while (check==false){
          //checks if the user inputs an int.
          if (sc.hasNextInt()==true){
            int temp=sc.nextInt();
            if((temp>=0&&temp<=100)&&(temp>=grades[x-1])){
              grades[x]=temp;
              check=true;
            }
            else {
              System.out.print("Please try again with a number between 0 and 100, in ascending order. ");
            }
          }
          else {
            System.out.print("Please try again with an int. ");
            sc.next();
          }
     }
 }
  printArray(grades,"");
  binary(grades);
  scramble(grades);
  printArray(grades, "scrambled ");
  linear(grades);
}
public static void printArray(int []grades, String addition){
  //prints Array
  System.out.print("Your "+addition+"grades are: ");
    for (int y=0; y<grades.length-1; y++){
      System.out.print(grades[y]+", ");
  }
  System.out.println();
}
public static void binary(int [] grades){
  //binary search for sorted arrays.
  Scanner sc = new Scanner(System.in);
  System.out.println("Enter a grade to search for: ");
  
  int search = sc.nextInt();
  int half = grades.length/2;  
  int start = 0;
  int length = grades.length-1;
  int counter=1;
    
        while (start <= length) {
          int half2 = (start + length) / 2;
            if (search == grades[half2]) {
                System.out.println(search+" was found at element "+ half2+" in the array with "+counter+" iterations.");
            }
            if (search < grades[half2]) {
                length = half2 - 1;
            } else {
                start = half2 + 1;
            }
          counter++;
        }
  
  int isPresent=0;
  for (int z=0; z<grades.length; z++){
    if (grades[z]!=search){
      isPresent++;
    }
  }
  if(isPresent==16){
    System.out.println("Your number, "+search+", was not found in the array with "+counter+" iterations.");
  }
    }
public static void scramble(int [] grades){
  //scrambles the array around
  Random ran=new Random();
  
  for (int x=0; x<15; x++){
    int index=ran.nextInt(x+1);
    int num=grades[index];
    grades[index]=grades[x];
    grades[x]=num;
  }
}
public static void linear(int[]grades){
  //searchs an array in a linear fashion.
  Scanner sc5=new Scanner(System.in);
  
  System.out.println("Enter a grade you would like to search for in the scrambled array. ");
  int search5=sc5.nextInt();
  
  int counter2=0;
  for (int t=0; t<grades.length-1; t++){
    if(grades[t]==search5){
      System.out.println("Your grade, "+search5+", was found at element "+t+" in the scrambled array.");
    }
    else{
      counter2++;
    }
  }
  if (counter2==grades.length-1){
    System.out.println("Your grade, "+search5+", was not found in the scrambled array.");
  }
}
}
