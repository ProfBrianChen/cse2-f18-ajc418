import java.util.Scanner;
public class tipCalculator {
  public static void main(String args[]){
    Scanner sc = new Scanner(System.in);
    System.out.println("What is your total bill?");
    double totalBill = sc.nextDouble();
    System.out.println("What percent tip do you desire to give?");
    double tip2 = sc.nextDouble();
    calculateTip(totalBill, tip2);
  }
  public static void calculateTip(double bill, double desiredTip){
    double actualTip=desiredTip/100;
    double totalBill2=bill*actualTip;
    double totalBill3=totalBill2+bill;
    System.out.println("Your total bill with "+desiredTip+"% tip is: "+totalBill3+".");
  }
}