//Andrew Conboy CSE2 Homework #2
//this program computes the cost of the items purchased at a store in PA w/ 6% sales tax.
public class Arithmetic{
  public static void main(String args[]){
    int numPants = 3; //number of pairs of pants
    double pantsPrice = 34.98; // price of each of the pairs of pants
    int numShirts = 2; // number of shirts purchased
    double shirtPrice = 24.99;
    int numBelts = 1; // number of belts purchased
    double beltCost = 33.99; //price of each belt
    double paSalesTax = 0.06; //sales tax of PA 6%
    
    //total cost of each respective type of item before sales tax.
    double totalPantsCost = numPants*pantsPrice;
    double totalShirtsCost = numShirts*shirtPrice;
    double totalBeltCost = numBelts*beltCost;
    
    //displays the cost total of each item.
    System.out.println("The total cost of your pants is: $"+totalPantsCost);
    System.out.println("The total cost of your shirts is: $"+totalShirtsCost);
    System.out.println("The total cost of your belts is: $"+totalBeltCost);
    
    //calculates total tax for each respective type of item.
    double pantsTax = totalPantsCost*paSalesTax;
    double shirtTax = totalShirtsCost*paSalesTax;
    double beltTax = totalBeltCost*paSalesTax;
    
    //displays just the tax for purchasing a certain type of item.
    System.out.println("The total tax on your pants is: $"+ rounder(pantsTax));
    System.out.println("The total tax on your shirts is: $"+rounder(shirtTax));
    System.out.println("The total tax on your belts is: $"+rounder(beltTax));
    
    //total cost of purchases before sales tax.
    double taxFreeTotal = totalPantsCost+totalShirtsCost+totalBeltCost;
    
    //total tax charged on complete order.
    double taxTotal = pantsTax+shirtTax+beltTax;
    
    //total of purchase including item cost and sales tax.
    double finalTotal = taxFreeTotal+taxTotal;
   
    //displays the totals in tax, orders and final total.
    System.out.println("The total cost of your items without tax is: $"+rounder(taxFreeTotal));
    System.out.println("The total tax on your complete purchase is: $"+rounder(taxTotal));
    System.out.println("The total price you will pay is: $"+rounder(finalTotal));
  }
  //this method takes an unrounded price as a parameter and returns a rounded price to 2 dp to match penny prices.
  public static double rounder (double unrounded){
    double rounded = Math.round(unrounded*100.0)/100.0;
    return rounded;
  }
}