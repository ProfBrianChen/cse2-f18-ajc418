// Cyclometer records the trip time, distance, speed and tire rotations for your bicycle
public class Cyclometer {
  public static void main(String[] args) {
    int secondsTrip1 = 480; //time in seconds of trip 1
    int secondsTrip2 = 3220; //time in seconds of trip 2
    int countsTrip1 = 1561; //
    int countsTrip2 = 9037; //
    double wheelDiameter = 27.0,// diameter of bike wheels
    PI=3.14159, //value of pi.
    feetPerMile = 5280, //number of feet in a mile
    inchesPerFoot=12, //number of inches in a foot
    secondsPerMinute=60; //number of seconds per minute
    double distanceTrip1, distanceTrip2, totalDistance; //distnaces of trips
    System.out.println("Trip 1 took "+ secondsTrip1/secondsPerMinute+" minutes and had "+countsTrip1+" counts.");
    System.out.println("Trip 2 took "+ secondsTrip2/secondsPerMinute+" minutes and had "+countsTrip2+" counts.");
    distanceTrip1=countsTrip1*wheelDiameter*PI; //distnace in inches
    distanceTrip1/=inchesPerFoot*feetPerMile; // gives distance in miles /= operator divides a by b and stores in a
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip2+distanceTrip1;
    System.out.println("Trip 1 was "+distanceTrip1+" miles.");
    System.out.println("Trip 2 was "+distanceTrip2+" miles.");
    System.out.println("The total distance of both trips was "+totalDistance+" miles.");
  }
}