/* Andrew Conboy - Homework 3 Part 1 - Rainfall calculator - 9/13/2018
This program takes input of an area (acres), and average rainfall (in) 
over that area to calculate total volume of rain. 
Work checked with https://www.vcalc.com/wiki/MichaelBartmess/Rainfall+Volume*/
import java.util.Scanner;
public class Convert {
  public static void main(String args[]){
    Scanner sc = new Scanner(System.in);
    //Asks for and stores the land area and average inches of rainfall over that area.
    System.out.print("How many acres of land were affected by hurricane precipitation?: ");
    double numAcres=sc.nextDouble();
    System.out.print("How many inches of rain were dropped on average?: ");
    double inchesRain=sc.nextDouble();
    
    //converts acres to square inches
    double acresToInches=numAcres*6273000;
   
    //computes cubic inches of rainfall
    double cubicInches=acresToInches*inchesRain;
    
    //converts cubic inches to cubic yards
    double cubicYards=cubicInches/46656;
    
    //converts cubic yards to cubic miles
    double cubicMiles=cubicYards/5451776;
    double cubicMiles2=cubicMiles/1000;
    
    //prints rainfall total
    System.out.println("The total volume of rain that fell over the "+numAcres+" acres is "+cubicMiles2+" cubic miles.");
    }
}