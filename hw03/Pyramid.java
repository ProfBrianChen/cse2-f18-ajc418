/* Andrew Conboy - Homework 3 Part 1 - Pyramid volume calculator - 9/13/2018
This program takes the square dimensions of a pyramid base and the pyramid's height 
to calculate the volume inside the pyramid'*/
import java.util.Scanner;
public class Pyramid{
  public static void main(String args[]){
  Scanner sc=new Scanner(System.in);
    //asks for and stores the pyramid's dimensions.
    System.out.print("What is the height of your pyramid in meters?: ");
    double height = sc.nextDouble();
    System.out.print("What is the length of one of the base sides of your pyramid in meters?: ");
    double length=sc.nextDouble();
    
    //calculates and outputs the volume using the formula of LWH/3
    double volume=(length*length*height/3);
    System.out.println("The volume of your pyramid is: "+volume+" cubic meters.");
  }
}