/* Andrew Conboy HW 04 -CrapsSwitch 
this program rolls 2 die or takes input from a user of 2 numbers between 1-6, and prints out a slang term based on the two numbers, using only switch statements. */

import java.util.Scanner;
public class CrapsSwitch{
  public static void main (String args[]){
    Scanner sc=new Scanner(System.in);
    int firstRoll=0;
    int secondRoll=0;
    
    //Prompts user for either the roll or input option
    System.out.print("Would you like to 'roll' 2 die or 'input' two numbers between 1 - 6?: ");
    String answer=sc.nextLine();
    
    //If the user enters roll, random numbers between 1 and 6 will be generated.
    switch (answer) {
      case "roll": firstRoll = (int)(Math.random()*6+1);
      secondRoll=(int)(Math.random()*6+1);
      System.out.print("Your numbers are: "+firstRoll+", "+secondRoll+". Your slang term is... ");
        break;
        
    //If the user enters input, they will be prompted to enter 2 numbers between 1 and 6.
      case "input": System.out.print("What is your first number? (0-6) : ");
                      firstRoll=sc.nextInt();
                    System.out.print("What is your second number? (0-6) : ");
                      secondRoll=sc.nextInt();
      System.out.print("Your numbers are: "+firstRoll+", "+secondRoll+". Your slang term is... ");
           default: break;
    }
          
    //Determines slang terminology based on roll numbers.
    switch (firstRoll){
      case 1: switch(secondRoll){
        case 1: System.out.println("Snake Eyes");
          break;
        case 2: System.out.println("Ace Deuce");
          break;
        case 3: System.out.println("Easy Four");
          break;
        case 4: System.out.println("Fever Five");
          break;     
        case 5: System.out.println("Easy Six");
          break;
        case 6: System.out.println("Seven Out");
          break;
        default: System.out.println("Invalid number. Please re-run the program and select numbers between 0-6.");
           break;
      }
      break;
       case 2: switch(secondRoll){
        case 1: System.out.println("Ace Deuce");
          break;
        case 2: System.out.println("Hard Four");
          break;
        case 3: System.out.println("Fever Five");
          break;
        case 4: System.out.println("Easy Six");
          break;     
        case 5: System.out.println("Seven Out");
          break;
        case 6: System.out.println("Easy Eight");
          break;
         default: System.out.println("Invalid number. Please re-run the program and select numbers between 0-6.");
           break;
      }
      break;
       case 3: switch(secondRoll){
        case 1: System.out.println("Easy Four");
          break;
        case 2: System.out.println("Fever Five");
          break;
        case 3: System.out.println("Hard Six");
          break;
        case 4: System.out.println("Seven Out");
          break;     
        case 5: System.out.println("Easy Eight");
          break;
        case 6: System.out.println("Nine");
          break;
         default: System.out.println("Invalid number. Please re-run the program and select numbers between 0-6.");
           break;
      }
      break;
       case 4: switch(secondRoll){
        case 1: System.out.println("Fever Five");
          break;
        case 2: System.out.println("Easy Six");
          break;
        case 3: System.out.println("Seven Out");
          break;
        case 4: System.out.println("Hard Eight");
          break;     
        case 5: System.out.println("Nine");
          break;
        case 6: System.out.println("Easy Ten");
          break;
         default: System.out.println("Invalid number. Please re-run the program and select numbers between 0-6.");
           break;
      }
      break;
       case 5: switch(secondRoll){
        case 1: System.out.println("Easy Six");
          break;
        case 2: System.out.println("Seven Out");
          break;
        case 3: System.out.println("Easy Eight");
          break;
        case 4: System.out.println("Nine");
          break;     
        case 5: System.out.println("Hard Ten");
          break;
        case 6: System.out.println("Yo-leven");
          break;
         default: System.out.println("Invalid number. Please re-run the program and select numbers between 0-6.");
           break;
      }
      break;
       case 6: switch(secondRoll){
        case 1: System.out.println("Seven Out");
          break;
        case 2: System.out.println("Easy Eight");
          break;
        case 3: System.out.println("Nine");
          break;
        case 4: System.out.println("Easy Ten");
          break;     
        case 5: System.out.println("Yo-leven");
          break;
        case 6: System.out.println("Boxcars");
          break;
         default: System.out.println("Invalid number. Please re-run the program and select numbers between 0-6.");
           break;
      }
     default: System.out.println("Invalid number(s). Please re-run the program and select numbers between 0-6.");
        break;
    }
  }
}