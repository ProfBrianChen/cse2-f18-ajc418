/* Andrew Conboy - HW 04 - CrapsIf 
This program rolls 2 die or takes input from a user of 2 numbers between 1-6, and prints out a slang term based on the two numbers, using only if statements. */
import java.util.Scanner;
public class CrapsIf{
  public static void main (String args[]){
    Scanner sc=new Scanner(System.in);
    int firstRoll=0;
    int secondRoll=0;
    
    //Prompts user for either the roll or input option
    System.out.print("Would you like to 'roll' 2 die or 'input' two numbers between 1 - 6?: ");
    String answer=sc.nextLine();
    
    //If the user enters roll, random numbers between 1 and 6 will be generated.
    if (answer.equals("roll")) {
      firstRoll=(int)(Math.random()*6+1);
      secondRoll=(int)(Math.random()*6+1);
      System.out.print("Your numbers are: "+firstRoll+", "+secondRoll+". Your slang term is... ");
     }
    
    //If the user enters input, they will be prompted to enter 2 numbers between 1 and 6.
    else if (answer.equals("input")) {
      System.out.print("What is your first number? (0-6): ");
      firstRoll=sc.nextInt();
    
    //gives user one additional chance to pick a number between 1 and 6.   
          if (firstRoll>6){
            System.out.print("Try again. Please enter a number between 1 and 6: ");
            firstRoll=sc.nextInt();
          }
      System.out.print("What is your second number? (0-6): ");
      secondRoll=sc.nextInt();
      
     //gives user one additional chance to pick a number between 1 and 6.
          if (secondRoll>6){
            System.out.println("Try again. Please enter a number between 1 and 6: ");
            secondRoll=sc.nextInt();
          }
       } else {
      System.out.println("Try again. Please enter either 'roll' or 'input'.");
    }
    
    //Determines and prints slang terminology based on roll numbers.
    if (firstRoll ==1 && secondRoll == 1){
      System.out.println("Snake Eyes");
    }
    else if ((firstRoll ==1 || secondRoll == 1) && (firstRoll ==2 || secondRoll ==2)){
      System.out.println("Ace Deuce");
    }
    else if ((firstRoll==1 || secondRoll == 1) && (firstRoll==3 || secondRoll ==3)){
      System.out.println("Easy Four");
    }
    else if (((firstRoll ==1|| secondRoll == 1) && (firstRoll==4 || secondRoll ==4)) || ((firstRoll==2 || secondRoll == 2) && (firstRoll==3|| secondRoll ==3))) {
      System.out.println("Fever Five");
    }
    else if ((firstRoll==1 || secondRoll == 1) && (firstRoll==5 || secondRoll ==5)){
      System.out.println("Easy Six");
    }
    else if (((firstRoll==1 || secondRoll == 1) && (firstRoll==6 || secondRoll ==6)) || ((firstRoll==2 || secondRoll == 2) && (firstRoll==5 || secondRoll ==5)) || ((firstRoll==4 || secondRoll == 4) && (firstRoll==3 || secondRoll ==3))){ 
      System.out.println("Seven Out");
    }
    else if (firstRoll==2 && secondRoll ==2){
      System.out.println("Hard four");
    }
    else if ((firstRoll==2 || secondRoll == 2) && (firstRoll==4 || secondRoll ==4)){
      System.out.println("Easy Six");
    }
    else if (((firstRoll==2 || secondRoll == 2) && (firstRoll ==6 || secondRoll ==6)) || ((firstRoll==5 || secondRoll == 5) && (firstRoll==3 || secondRoll ==3))){
      System.out.println("Easy Eight");
    }
    else if (((firstRoll==3 || secondRoll == 3) && (firstRoll==6 || secondRoll ==6)) || ((firstRoll==5 || secondRoll == 5) && (firstRoll==4 || secondRoll ==4))){
      System.out.println("Nine");
    }
    else if (firstRoll==3 && secondRoll ==3){
      System.out.println("Hard Six");
    }
    else if (firstRoll==4 && secondRoll ==4){
      System.out.println("Hard Eight");
    }
    else if ((firstRoll==4 || secondRoll == 4) && (firstRoll==6 || secondRoll ==6)){
      System.out.println("Easy Ten");
    }
    else if (firstRoll==5 && secondRoll ==5){
      System.out.println("Hard Ten");
    }
    else if ((firstRoll==5 || secondRoll == 5) && (firstRoll==6 || secondRoll ==6)){
      System.out.println("Yo-leven");
    }
    else if (firstRoll==6 && secondRoll ==6){
      System.out.println("Boxcars");
    }
    else{
      System.out.println("Invalid numbers. Try again.");
    }
  }
}