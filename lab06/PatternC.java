import java.util.Scanner;
public class PatternC {
  public static void main (String [] args){
    Scanner sc=new Scanner(System.in);
    int pyrRows=0;
    
    //checks if user enters int between 1-10
    boolean check=false;
    while (check==false){
      System.out.print("How many lines do you want your pyramid to be? Enter an integer between 1-10: ");
      check=sc.hasNextInt();
      if(check){
        pyrRows=sc.nextInt();
        if (pyrRows<=10&&pyrRows>=1){
          break;
        }
        else{
          check=false;
        }
      }
      else {
        sc.next();
      }
    }
    
  for (int col=0; col<pyrRows; col++){
    for(int space=pyrRows; space>col; space--){
      System.out.print(" ");
    }
        System.out.print(col+1);
    for (int row=col; row>0; row--){
      System.out.print(row);
      }
      System.out.println("");
    }
  }
}