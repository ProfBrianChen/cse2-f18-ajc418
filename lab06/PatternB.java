import java.util.Scanner;
public class PatternB {
  public static void main (String [] args){
    Scanner sc=new Scanner(System.in);
    int pyrRows=0;
    
    //checks if user enters int between 1-10
    boolean check=false;
    while (check==false){
      System.out.print("How many lines do you want your pyramid to be? Enter an integer between 1-10: ");
      check=sc.hasNextInt();
      if(check){
        pyrRows=sc.nextInt();
        if (pyrRows<=10&&pyrRows>=1){
          break;
        }
        else{
          check=false;
        }
      }
      else {
        sc.next();
      }
    }
    
    for (int row=pyrRows; row>0; row--){
      for (int col=0; col<row; col++){
        System.out.print(col+1);
      }
      //System.out.print(row);
      System.out.println("");
    }
  }
}
