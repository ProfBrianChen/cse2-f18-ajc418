//Andrew Conboy HW08 - Card shuffler and drawer. This program generates a deck, shuffles it, and prints out x amounts of hands.
import java.util.Scanner;
public class hw08{ 
public static void main(String[] args) { 
  Scanner scan = new Scanner(System.in); 
  //suits club, heart, spade or diamond 
  String[] suitNames={"C","H","S","D"};    
  String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
  String[] cards = new String[52]; 
  String[] hand = new String[5]; 
  int numCards = 5; 
  int again = 1; 
  int index = 51;
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
      //System.out.print(cards[i]+" "); 
    } 
  //System.out.println();
  printArray(cards); 
  shuffle(cards); 
  printArray(cards); 
    while(again == 1){
      //this if statement checks how many cards are left. If the desired hand is greater than the amount of cards left, a new deck is generated.
      if (numCards>index){
        index=51;
        shuffle(cards);
        System.out.println("A new deck was generated.");
      }
      else{
       hand = getHand(cards,index,numCards); 
       printArray(hand);
       index = index-numCards;
       System.out.println("Enter a 1 if you want another hand drawn. Enter 0 to exit."); 
       again = scan.nextInt(); 
      }
    }
  }
public static void printArray(String [] deck){
  for(int x=0; x<deck.length; x++){
    System.out.print(deck[x]+" ");
  }
  System.out.println();
}
public static String[] shuffle (String [] orderedDeck){
  //this method shuffles the deck by swapping elements over 100 times.
  System.out.println("Shuffled: ");
  for(int x=0; x<orderedDeck.length*2; x++){
    int slook = (int)(Math.random()*51+1);
    //System.out.println(slook+", ");
    String num=orderedDeck[0];
    orderedDeck[0]=orderedDeck[slook];
    orderedDeck[slook]=num;
  }
  return orderedDeck;
}
public static String[] getHand(String [] deck, int index, int numCards){
  //this method prints out a hand that is numCards in length
  System.out.println("Hand: ");
  String [] hand2 = new String [numCards];
  for (int z=0; z<numCards; z++){
    hand2[z]=deck[index-z];
  }
  return hand2;
}
}