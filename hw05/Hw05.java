import java.util.Scanner;
public class Hw05 {
  public static void main (String [] args){
    //initialize scanner object
    Scanner sc = new Scanner(System.in);
    
    //initialize
    int hands = 0;
    int king=0, ace=0, two=0, three=0, four=0, five=0, six=0, seven=0, eight=0, nine=0, ten=0, jack=0, queen=0;

    // asks user how many times they want to generate hands
    boolean isInt = false;
    while (isInt == false){
      System.out.println("How many times do you want to generate a random poker hand?: ");
      isInt = sc.hasNextInt();
      if (isInt){
        hands=sc.nextInt();
      }
      else{
        sc.next();
      }
     }
    int handsCounter = 0;
    double fourKind=0, threeKind=0, pairKind=0;
    while (handsCounter<hands){
      int [] cardArray = new int [5];
      
      //assigns 5 cards to each hand
      int x=0;
      while (x<5)
        {
          cardArray [x] = (int) (Math.random() * 52 +1);
          if (assignNumber(cardArray[x]).equals("King")){
              king++;
            }
            else if (assignNumber(cardArray[x]).equals("Ace")){
              ace++;
            }
            else if (assignSuit(cardArray[x]).equals("2")){
              two++;
            }
            else if (assignNumber(cardArray[x]).equals("3")){
              three++;
            }
            else if (assignNumber(cardArray[x]).equals("4")){
              four++;
            }
            else if (assignNumber(cardArray[x]).equals("5")){
              five++;
            }
            else if (assignNumber(cardArray[x]).equals("6")){
              six++;
            }
            else if (assignNumber(cardArray[x]).equals("7")){
              seven++;
            }
            else if (assignNumber(cardArray[x]).equals("8")){
              eight++;
            }
            else if (assignNumber(cardArray[x]).equals("9")){
              nine++;
            }
            else if (assignNumber(cardArray[x]).equals("10")){
              ten++;
            }
            else if (assignNumber(cardArray[x]).equals("Jack")){
              jack++;
            }
            else if (assignNumber(cardArray[x]).equals("Queen")){
              queen++;
            }
            else {
              System.out.print("");
            }
      //System.out.print(assignNumber(cardArray[x])+" of "+assignSuit(cardArray[x])+", ");
        x++;
      }
      if (king==4|ace==4|two==4|three==4|four==4|five==4|six==4|seven==4|eight==4|nine==4|ten==4|jack==4|queen==4){
        fourKind++;
        //System.out.print(".Your hand has 'Four of a kind'!");
      }
        else if (king==3|ace==3|two==3|three==3|four==3|five==3|six==3|seven==3|eight==3|nine==3|ten==3|jack==3|queen==3){
          threeKind++;
          //System.out.print(". Your hand has 'Three of a kind'!");
        }
        else if (king==2|ace==2|two==2|three==2|four==2|five==2|six==2|seven==2|eight==2|nine==2|ten==2|jack==2|queen==2){
          pairKind++;
          //System.out.print(". Your hand has 'One Pair'!");
        }
      handsCounter++;
    }
System.out.println("You simulated drawing "+hands+" hands.");
System.out.println("The probability of getting 'four of a kind' is: "+fourKind/hands);
System.out.println("The probability of getting 'three of a kind' is: "+threeKind/hands);
System.out.println("The probability of getting 'one pair' is: "+ pairKind/hands);
System.out.println("The probability of getting '2 pair' is: 0");
  }
  /*this method takes the random number assigned in the main method and assigns
  a suit based on where the number falls.*/
  public static String assignSuit (int card) {
    String suit;
    if (card <=13){
      suit = "diamonds";
    }
    else if (card <=26 && card>=14){
      suit = "clubs";
    }
    else if (card <=39 && card>=27){
      suit = "hearts";
    }
    else {
      suit = "spades";
    }
    return suit;
  }
  
  /*this method takes the random number, divides it by 14, and takes the remainder
  to assign a card number. Since 13 cards are in each suit, %14 was used. */
  public static String assignNumber (int num){
    String cardNumber="null";
    int adjustedNum = num%13;
    switch(adjustedNum){
      case 0: cardNumber="King";
        break;
      case 1: cardNumber= "Ace";
        break;
      case 2: cardNumber= "2";
        break;
      case 3: cardNumber= "3";
        break;
      case 4: cardNumber= "4";
        break;
      case 5: cardNumber= "5";
        break;
      case 6: cardNumber= "6";
        break;
      case 7: cardNumber= "7";
        break;
      case 8: cardNumber= "8";
        break;
      case 9: cardNumber= "9";
        break;
      case 10: cardNumber= "10";
        break;
      case 11: cardNumber= "Jack";
        break;
      case 12: cardNumber= "Queen";
        break;
    }
    return cardNumber;
  }
}